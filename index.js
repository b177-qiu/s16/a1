let number = Number(prompt("Please input number."));

console.log("The number you provided is " + number)

for(let i = number; i >= 50; i--){
	if (i === 50){
		console.log("The current value is at 50. Terminating loop.");
		continue;
	}

	if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (i % 5 === 0){
		console.log(i);
		continue;
	}
}


let word = "supercalifragilisticexpialidocious";
let consonants = "";

	console.log(word);

for(let i = 0; i < word.length; i++){

	if (word[i].toLowerCase() == "a" ||
		word[i].toLowerCase() == "e" ||
		word[i].toLowerCase() == "i" ||
		word[i].toLowerCase() == "o" ||
		word[i].toLowerCase() == "u" 
	){
		continue;
	}
	else{
		consonants += word[i] + "";
	}
}

	console.log(consonants);
